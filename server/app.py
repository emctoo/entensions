#!/usr/bin/env python
# coding: utf8

import bottle, json

app = bottle.Bottle()

@app.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

@app.route('/data.json', method='GET')
def onDataJson():
    return 'hello, world!'


@app.route('/links', method='POST')
def onLinks():
    links = json.loads(bottle.request.forms.get('links'))
    print(links)
    return 'ok'

bottle.run(app, host='0.0.0.0', port=8000, reloader=True)
