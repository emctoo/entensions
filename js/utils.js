function httpRequest(url, callback){
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      callback(xhr.responseText);
    }
  }
  xhr.send();
}

/*
httpRequest('http://localhost:8000/data.json', function(ip){
  document.getElementById('ip_div').innerText = ip;
});
*/
document.getElementById('ip_div').innerText = "welcome";
